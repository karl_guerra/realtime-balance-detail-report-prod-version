package main;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import com.tlc.common.DbWrapper;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class MainFile {

	public static void main(String[] args) throws Exception {
		

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
        Date date = new Date();
        String currDate = formatter.format(date);
        System.out.println("Current Date: "+currDate);
		
		System.out.println("GETTING MSISDN & BALANCE FROM TBLCURRENTSTOCK, Please Wait....");
		System.out.println("");
		try {
			DbWrapper db = new DbWrapper();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con=db.GetConnection();
			
			System.out.println("--------------------------------------");
	        System.out.println("| Getting Records FROM CURRENTSTOCK! |");
	        System.out.println("--------------------------------------");
	        
	        /*
			 * Old crypt code: "FhTQJUjwC4BmLBYwqKm"
			 * New crypt code (Test environment): "7JZwHGD48kqPpPXn"
			 * New crypt code (Prod environment): "JfS7knPaBSTWVCGZ"
			 */
	        
	        String st = "SELECT ID, MSISDN,Decrypt(AMOUNT, 'JfS7knPaBSTWVCGZ', MSISDN)/100 AS BALANCE, UPDATEDDATE, EVD, WALLETID FROM TBLCURRENTSTOCK WHERE WALLETID = 0";
			Statement stmt=con.createStatement();  
			ResultSet rs = stmt.executeQuery(st);
			
			List<List<String>> rows = new ArrayList<>();
			while(rs.next()) {
				
				String amnt = rs.getString(3), 
					amount = "";
				if(amnt == null) {
					amount = "0";
				}
				else {
					amount = amnt;
				}
				System.out.println(rs.getString(1)+" "+rs.getString(2)+"  "+amount+" "+rs.getString(4)+" "+rs.getString(5)+" "+rs.getString(6));
				String[] ar = {rs.getString(1),rs.getString(2),amount,rs.getString(4),rs.getString(5),rs.getString(6)};
				List<String> af = Arrays.asList(ar);
				rows.add(af);
			}
			con.close();
			System.out.println("-------------------------------------------------------");
			System.out.println("| Now Exporting 'Report "+ currDate +".xslx' file.... |");
			System.out.println("-------------------------------------------------------");
			
			Export(rows, currDate);
			
			System.out.println("");
			System.out.println("---------------------");
			System.out.println("| Status: Finished! |");
			System.out.println("---------------------");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
	}

	public static void Export(List<List<String>> rows, String currDate) {
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
        
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ID", "MSISDN", "BALANCE", "UPDATED DATE", "EVD", "WALLETID"});
        int ctr = 2;
        for (List<String> rowData : rows) {
		    
        	data.put(""+ctr, new Object[] {rowData.get(0),rowData.get(1),rowData.get(2),rowData.get(3),rowData.get(4),rowData.get(5)});
        	ctr++;
        }
        
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("Report "+currDate+".xlsx"));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
	}
	
	public static void Export2(List<List<String>> rows, String currDate) {
		
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
        
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
        
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ID", "MSISDN", "BALANCE", "UPDATED DATE", "EVD", "WALLETID"});
        int ctr = 2;
        for (List<String> rowData : rows) {
		    
        	data.put(""+ctr, new Object[] {rowData.get(0),rowData.get(1),rowData.get(2),rowData.get(3),rowData.get(4),rowData.get(5)});
        	ctr++;
        }
        
        //Iterate over data and write to sheet
        Set<String> keyset = data.keySet();
        int rownum = 0;
        for (String key : keyset)
        {
            Row row = sheet.createRow(rownum++);
            Object [] objArr = data.get(key);
            int cellnum = 0;
            for (Object obj : objArr)
            {
               Cell cell = row.createCell(cellnum++);
               if(obj instanceof String)
                    cell.setCellValue((String)obj);
                else if(obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
            }
        }
        
        try
        {
            //Write the workbook in file system
            FileOutputStream out = new FileOutputStream(new File("Report "+currDate+".xlsx"));
            workbook.write(out);
            out.close();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
	}
}
